
const eyeIcons = document.querySelectorAll('.fas');
const btn = document.querySelector('.btn');

eyeIcons.forEach(elem => {
    elem.addEventListener('click', ev => {
        const currentInput = ev.target.closest('label').querySelector('input');
        elem.classList.toggle('fa-eye-slash');
        if (elem.classList.contains('fa-eye-slash')){
            currentInput.type = '';
        } else {
            currentInput.type = 'password';
        }
    })
});

btn.addEventListener('click', evnt => {
    evnt.preventDefault();
    debugger
    const password = document.querySelector('#password').value;
    const repeatPassword = document.querySelector('#repeat-password').value;
    const error = document.querySelector('.error');
    if(password && repeatPassword && password === repeatPassword) {
        alert('You are welcome');
    } else {
        error.style.color = 'red';
        error.innerText = 'Потрібно ввести однакові значення';
    }
})
